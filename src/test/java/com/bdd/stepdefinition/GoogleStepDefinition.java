package com.bdd.stepdefinition;

import com.bdd.step.GoogleStep;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import cucumber.api.java.es.Y;
import net.thucydides.core.annotations.Steps;

public class GoogleStepDefinition {

    @Steps
    GoogleStep googleStep;

    @Dado("^que abro Google$")
    public void que_abro_google(){
        googleStep.que_abro_google();
    }

    @Cuando("^busco la palabra \"([^\"]*)\"$")
    public void busco_la_palabra(String searchTerm){
        googleStep.busco_la_palabra(searchTerm);
    }

    @Y("^doy clic en el enlace de Wikipedia$")
    public void doy_clic_en_el_enlace_de_Wikipedia(){
        googleStep.doy_clic_en_el_enlace_de_Wikipedia();
    }

    @Y("^he tomado una captura de pantalla de la página$")
    public void he_tomado_captura(){
        googleStep.he_tomado_captura();
    }

    @Entonces("^verifico el año del primer proceso automático en Wikipedia$")
    public void verifico_el_ano_del_primer_proceso_automatico(){
        googleStep.verifico_el_ano_del_primer_proceso_automatico();
    }

}
