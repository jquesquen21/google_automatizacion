package com.bdd.step;

import com.bdd.page.GooglePage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class GoogleStep extends ScenarioSteps {

    GooglePage googlePage;

    @Step
    public void que_abro_google(){
        googlePage.que_abro_google();
    }

    @Step
    public void busco_la_palabra(String searchTerm){
        googlePage.busco_la_palabra(searchTerm);
    }

    @Step
    public void doy_clic_en_el_enlace_de_Wikipedia(){
        googlePage.doy_clic_en_el_enlace_de_Wikipedia();
    }

    @Step
    public void he_tomado_captura(){
        googlePage.he_tomado_captura();
    }

    @Step
    public void verifico_el_ano_del_primer_proceso_automatico(){
        googlePage.verifico_el_ano_del_primer_proceso_automatico();
    }

}
