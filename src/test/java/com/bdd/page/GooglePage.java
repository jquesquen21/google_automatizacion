package com.bdd.page;

import net.serenitybdd.core.Serenity;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.WebElement;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.Keys;

import java.util.NoSuchElementException;
import org.openqa.selenium.JavascriptExecutor;

@DefaultUrl("https://www.google.com/")
public class GooglePage extends PageObject {

    @FindBy(name = "q")
    private WebElement searchBox;

    public void que_abro_google(){
        String navegador = "chrome";
        Browser.Start(this,navegador);
    }

    public void busco_la_palabra(String searchTerm) {
        searchBox.sendKeys(searchTerm);
        searchBox.sendKeys(Keys.ENTER);
    }

    public void doy_clic_en_el_enlace_de_Wikipedia() {
        find(By.xpath("//a[@href='https://es.wikipedia.org/wiki/Automatizaci%C3%B3n']")).click();
    }

    public void he_tomado_captura(){
        Serenity.takeScreenshot();
    }

    public void verifico_el_ano_del_primer_proceso_automatico() {

        try {
            WebElement elemento = find(By.xpath("//a[contains(text(),'populista')]"));
            ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", elemento);

        } catch (NoSuchElementException e) {
            throw new RuntimeException("No se pudo encontrar el elemento 'populista'.");
        }

    }

}
