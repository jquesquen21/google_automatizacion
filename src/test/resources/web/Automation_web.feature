#language: es
@GoogleDemo
Característica: Web Demo

  Antecedentes: Búsqueda de Google y verificación de Wikipedia

  @SearchGoogle
  Esquema del escenario: : Verificar primer año de proceso automático en Wikipedia
    Dado que abro Google
    Cuando busco la palabra "<search>"
    Y doy clic en el enlace de Wikipedia
    Entonces verifico el año del primer proceso automático en Wikipedia
    Y he tomado una captura de pantalla de la página

    Ejemplos:
    |search|
    |automatizacion|



